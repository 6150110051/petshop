package com.plusitsolution.animalcare.service;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;

import com.plusitsolution.animalcare.domain.AnimalCareDomain;



@Service
public class AnimalCareService {
	
	Map<Long, AnimalCareDomain> animalMap = new HashMap<>();
	
//	List<AnimalCareDomain> list = new ArrayList<>();
//    Map<Long,AnimalCareDomain> map = new HashMap<Long,AnimalCareDomain>(); //Object is containing String
//    Map<Long,String> newMap =new HashMap<Long,String>();
//    for (Map.Entry<String, Object> entry : map.entrySet()) {
//           if(entry.getValue() instanceof String){
//                newMap.put(entry.getKey(), (String) entry.getValue());
//              }
//     }
//	List<Map<Long, AnimalCareDomain>>  mapList=new ArrayList<Map<Long, AnimalCareDomain>>();
//
//    List<AnimalCareDomain> list=new ArrayList<AnimalCareDomain>();
//    for(Map<Long,AnimalCareDomain> i:mapList){
//        list.addAll(i.values());
//    }
//	ArrayList<Long> animalMap1 = new ArrayList<>(animalMap.keySet());
//	ArrayList<Value> animalMap2 = new ArrayList<>(animalMap.values());
//	animalMap1.put(animalMap);
	 
	
	private static AtomicLong counter = new AtomicLong();
	

	
	public void registerAnimalCare(String ownerName, String animalSpecies, String phone, LocalDateTime timeDate) {
		long idAnimal = counter.incrementAndGet();
		AnimalCareDomain domain = new AnimalCareDomain(idAnimal, ownerName, animalSpecies, phone, timeDate);
		animalMap.put(idAnimal, domain);
	}
	
	public void editAnimalCare(Long id, String ownerName, String animalSpecies, String phone) {
		AnimalCareDomain domain = new AnimalCareDomain(id, ownerName, animalSpecies, phone, LocalDateTime.now());
		animalMap.put(id, domain);
		
		if(id == null) {
			throw new IllegalArgumentException("This id does not exist in the data.");
		}
	}
	

	public AnimalCareDomain getAnimalSearchByid(long id) {
		AnimalCareDomain domain = animalMap.get(id);
		if(domain == null) {
			throw new IllegalArgumentException("This id does not exist in the data.");
		} 
		return domain;
	}
	
	public AnimalCareDomain deleteAnimalCare(long id) {
		AnimalCareDomain domain = animalMap.remove(id);
		
		
		if(domain == null) {
			throw new IllegalArgumentException("id not found");
		}else {
		
			throw new IllegalArgumentException("delete successful");
		}
	}
	
	
	public List<AnimalCareDomain> getAnimalList(){
        List<AnimalCareDomain> list = new ArrayList<>();
        animalMap.entrySet().stream().forEach( domain -> {
            list.add(domain.getValue());
        });
        return list;
    }
	
	
	
//	public String uplondProfilePicture(MultipartFile file)  {
//		if (file == null) {
//			// throw error
//		 	 throw new IllegalArgumentException("file.null");
//		}
//		
//		if (file.getSize() > 1048576 * 2 ) { //ตรวจสอบว่ามากกว่า 2 mb
//			// throw error
//			throw new IllegalArgumentException("file.max.size");
//		}
//		
//		String contenType = file.getContentType() ;//สร้างการเก็บตัวแปร
//		if (contenType == null) {
//			// throw error
//			throw new IllegalArgumentException("unsupportedTypes.file.type");
//		}
//		
//		List<String> supportedTypes = Arrays.asList("image/jpg", "image/png");//ทำการตรวจสอบสกุลไฟล์ จาก contenType
//		if (supportedTypes.contains(contenType)) {
//			// throw error
//			throw new IllegalArgumentException("unsupportedTypes.file.type");
//		}
//		
//		try {
//			byte[] bytes = file.getBytes(); // อัพโหลดเข้า sever
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return "";
//		
//	}
//	@PostConstruct
//	private void init() {
//		animalMap.put((long) 1, new AnimalCareDomain((long) 1, "Pond", "Doge", "089-6460375", LocalDateTime.now()));
//		animalMap.put((long) 2, new AnimalCareDomain((long) 2, "Dee", "Cat", "089-0000000", LocalDateTime.now()));
//	}

}