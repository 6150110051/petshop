package com.plusitsolution.animalcare.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.plusitsolution.animalcare.domain.AnimalCareDomain;
import com.plusitsolution.animalcare.service.AnimalCareService;

@RestController
public class AnimalCareConrtoller {

	@Autowired
	private AnimalCareService service;
	
	


	@PostMapping("/registerAnimalCare")
	public void registerAnimalCare(@RequestParam("ownerName") String ownerName,@RequestParam("animalSpecies") String animalSpecies, @RequestParam("phone") String phone) {
		
		service.registerAnimalCare( ownerName, animalSpecies, phone, LocalDateTime.now());
	}
	@PutMapping("/editgisterAnimalCare/{id}")
	public void editAnimalCare(@RequestParam("ownerName") String ownerName,@RequestParam("animalSpecies") String animalSpecies, @RequestParam("phone") String phone,@PathVariable("id") long id) {
		service.editAnimalCare(id, ownerName, animalSpecies, phone);
	}
	@DeleteMapping("/deleteAnimalCare/{id}")
	public void deleteAnimalCare(@PathVariable("id") long id) {
		service.deleteAnimalCare(id);
	}
	@GetMapping("/AnimalSearchByid")
	public AnimalCareDomain getAnimalSearchByid(@RequestParam("id") long id) {
		return service.getAnimalSearchByid(id);
	}
	@GetMapping("/AnimalList")
    public List<AnimalCareDomain> getAnimalList(){
        return service.getAnimalList();
    }
//	}

//	@PostMapping
//	public ResponseEntity<String> uploadProfilePicture(@RequestPart MultipartFile file) {
//		String response = service.uplondProfilePicture(file);
//	}
	
}