package com.plusitsolution.animalcare.domain;


import java.time.LocalDateTime;

public class AnimalCareDomain {
	private Long id;
	private String  ownerName;
	private String animalSpecies;
	private String  phone;
	private LocalDateTime timeDate;
	
	
	public AnimalCareDomain(String phone) {	
		this.phone = phone;
	}
	
	public void editCareDomain(Long id, String ownerName, String animalSpecies, String phone, LocalDateTime timeDate) {
//		super();
		this.id = id;
		this.ownerName = ownerName;
		this.animalSpecies = animalSpecies;
		this.phone = phone;
		this.timeDate = timeDate;
	}
	

	public AnimalCareDomain(Long id, String ownerName, String animalSpecies, String phone, LocalDateTime timeDate) {
		super();
		this.id = id;
		this.ownerName = ownerName;
		this.animalSpecies = animalSpecies;
		this.phone = phone;
		this.timeDate = timeDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getAnimalSpecies() {
		return animalSpecies;
	}

	public void setAnimalSpecies(String animalSpecies) {
		this.animalSpecies = animalSpecies;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public LocalDateTime getTimeDate() {
		return timeDate;
	}

	public void setTimeDate(LocalDateTime timeDate) {
		this.timeDate = timeDate;
	}
	
}